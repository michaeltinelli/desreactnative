import React, { Component } from 'react';
import { Text, View, StyleSheet, FlatList} from 'react-native';
import { colors } from '../common';
import TopBar from '../components/TopBar';
import Exercicio from '../components/Exercicio'
import datas from '../data/data.json'
import Atividade from '../components/Atividade';

export default class Exercicios extends Component {

  state = {
    exercices: datas.exercices,
    ativs: []
  }

  filterExercices = ativs => {
    
  }

  render() {
    return (
      <View style={styles.container}>
        <TopBar />
        <Atividade filters={datas.filters} onFilterExercices={this.filterExercices} />
        <FlatList data={this.state.exercices} keyExtractor={(_, index) => `${index}`} style={{width: '90%'}} 
        renderItem={({ item }) => <Exercicio datas={item}  />} scrollEnabled={true} 
         /> 
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 2,
      backgroundColor: colors.darkBlack,
      justifyContent: 'center',
      alignItems: 'center',
    }
})
