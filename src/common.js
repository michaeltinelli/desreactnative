export const colors = {
    white: '#FEFFFF',
    initGradient: '#7F38F4',
    endGradient: '#F22B48',
    lightGreen: '#19B996',
    lightRed: '#FD3C29',
    darkBlack: '#262F38',
    lightBlack: '#323C47',
}

export const fonts = {
        regular: require('../assets/fonts/Montserrat-Regular.ttf'),
        bold:  require('../assets/fonts/Montserrat-Bold.ttf'),
        light:  require('../assets/fonts/Montserrat-Light.ttf'),
        thin:  require('../assets/fonts/Montserrat-Thin.ttf'),
}