import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableWithoutFeedback, Image} from 'react-native';
import { Icon } from 'react-native-elements'
import { colors } from '../common';
import LinearGradient from 'react-native-linear-gradient'


export default class Filter extends Component {

  componentDidMount = () => {
      //console.log(this.state.source.concat(this.props.el.image))  
      //this.setState({ source: this.state.source.concat(this.props.el.image) })
  
  }

  state = {
    check: false,
    source: '../../assets/images/yoga.png',
  }

  filter = () => {
    this.setState({ check: !this.state.check })
    //this.props.onFilterExercices(this.props.el.name)
  }

  render() {
    return (
      <View>
        <TouchableWithoutFeedback onPress={() => this.filter()}>
        <LinearGradient style={styles.circleArea} colors={[colors.initGradient, colors.endGradient]}>
        
        {
            this.state.check && (
            <Icon type={'font-awesome'} size={20} name={'check-circle'} color={colors.lightGreen} 
            containerStyle={{marginLeft: 30,}} />)
        } 
        {/* <Image source={require(`${this.state.source}`)} /> */}
        <Text>{this.props.el.name}</Text>
        </LinearGradient>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    
    circleArea: {
      borderRadius: 100,
      width: 60,
      height: 60,
      marginLeft: 16,

        
    }
})
