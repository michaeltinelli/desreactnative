import React, { Component } from 'react';
import { Text, View, StyleSheet, Image} from 'react-native';
import { colors } from '../common';
import { Button, Divider } from 'react-native-elements';


export default class Informacoes extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={{justifyContent: 'center', alignItems: 'flex-start', padding: 18}}>
            <Text style={styles.title}>{this.props.name}</Text>
            <Divider style={{marginBottom: 4}} />
            <View style={styles.iconsArea}>
                <View style={styles.iconStyle}>
                    <Image source={require('../../assets/images/ic_bike.png')} style={{marginRight: 4}} />
                    <Text style={styles.text}>{this.props.calories} Kcal</Text>
                </View>
                <Text style={{color: colors.lightBlack}}>{'|'}</Text>
                <View style={styles.iconStyle}>
                    <Image source={require('../../assets/images/ic_time.png')} style={{marginRight: 4}} />
                    <Text style={styles.text}>{`${this.props.name === 'Musculação' 
                        && this.props.time > 59 ? `1 h` : `${this.props.time} m`}`}</Text>
                </View>
                <Text style={{color: colors.lightBlack}}>{'|'}</Text>
                <View style={styles.iconStyle}>
                    <Image source={require('../../assets/images/ic_balance.png')} style={{marginRight: 4}} />
                    <Text style={styles.text}>{this.props.weight} Kg</Text>
                </View>
            </View>
            <Divider style={{marginBottom: 4}} />
            <View style={styles.buttonsArea}>
                <Button readOnly titleStyle={{fontSize: 10}} title={'HOJE'} buttonStyle={[ styles.buttonStyle, { backgroundColor: 
                    this.props.when === 'today' ? colors.lightRed : colors.lightBlack, 
                    borderColor: this.props.when === '' ? 'gray' : colors.lightRed, }]} />
                <Button readOnly titleStyle={{fontSize: 10}} title={'ONTEM'} buttonStyle={[ styles.buttonStyle, { backgroundColor: 
                    this.props.when === 'yesterday' ? colors.lightGreen : colors.lightBlack,  
                    borderColor: this.props.when === '' ? 'gray' : colors.lightGreen, }]} />
            </View>
        </View>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.lightBlack,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        
    },
    title: {
        fontFamily: 'Montserrat-Bold.ttf',
        fontSize: 20,
        color: '#fff'
    },
    text: {
        fontFamily: 'Montserrat-Light.ttf',
        color: '#fff'
    },
    buttonStyle: {
        borderRadius: 18, 
        marginRight: 8, 
        height: '15%', 
        padding: 10,
        borderWidth: 1, 
    },
    iconStyle: {
        flexDirection: 'row', 
        justifyContent: 'flex-start', 
        alignItems: 'center', 
        marginRight: 8   
    },
    iconsArea: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center',
    },
    buttonsArea: {
        flexDirection: 'row', 
        justifyContent: 'flex-start', 
        alignItems: 'center',
    },
    
})
