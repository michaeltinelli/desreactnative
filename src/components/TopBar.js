import React, { Component } from 'react';
import { Text, View, StyleSheet} from 'react-native';
import { colors } from '../common';
import { Icon, Divider } from 'react-native-elements'

export default class TopBar extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.containerIcons}>
          <Icon type={'font-awesome'} name="bars" size={15} color="#fff" />
          <Text style={styles.title}>MEU PERFIL</Text>
          <Icon type={'font-awesome'} name="cog" size={15} color="#fff" />
        </View>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
     
      justifyContent: 'center',
      alignItems: 'center',
      width: '90%',
      padding: 5,
    },
    containerIcons: {
      backgroundColor: colors.darkBlack,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      width: '100%',
    },
    title: {
        fontFamily: 'Montserrat-Thin.ttf',
        fontSize: 30,
        color: '#fff'
    }
})
