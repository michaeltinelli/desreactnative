import React, { Component } from 'react';
import { View, StyleSheet, Image} from 'react-native';
import { colors } from '../common';
import Informacoes from './Informacoes';


export default class Exercicios extends Component {

  componentDidMount = () => {
    
  }

  render() {
    const { datas } = this.props
    return (
      <View style={styles.container}>
        <View style={styles.circleArea}>
         {/* <Image source={require(`../images/${datas.image}`)}  /> */}
        </View> 
        <View style={{justifyContent: 'center', alignItems: 'flex-end'}}>
          <Informacoes name={datas.name} when={datas.when} time={datas.time} 
          weight={datas.weight} calories={datas.calories} />
            
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      backgroundColor: colors.lightBlack,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      borderRadius: 8,
    },
    title: {
      fontFamily: 'Montserrat-Bold.ttf',
      fontSize: 20,
      color: '#fff'
    },
    circleArea: {
      backgroundColor: colors.darkBlack,
      borderRadius: 100,
      width: 90,
      height: 90,
      marginLeft: 10,
    }
})
