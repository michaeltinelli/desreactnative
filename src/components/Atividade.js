import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';

import { colors } from '../common';
import Filter from './Filter';

export default class Atividade extends Component {

  componentDidMount = () => {
    //console.log(this.props.filters)
  }

  

  render() {
    return (
      <View style={styles.container}>
          {
             this.props.filters.map((el, index) => {
              return (
                <Filter onFilterExercices={this.props.onFilterExercices} key={`${index}`} el={el} /> 
              )
             }) 
          }
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      
      backgroundColor: colors.lightBlack,
      flexDirection: 'row',
      justifyContent: 'flex-start',
      alignItems: 'center',
      borderRadius: 8,
      width: '90%',
      padding: 20,
      margin: 20,
    },
    circleArea: {
      borderRadius: 100,
      width: 60,
      height: 60,
      marginLeft: 16,

        
    }
})
